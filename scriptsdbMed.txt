
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Roberto", "Flores", "1995-01-01", "calle libertad",75654215,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Jose Carlos", "Padilla", "1989-05--18", "calle amboro",72854521,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Cris", "Moron", "1997-09-08", "zona los pozos",77365986,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Maria A", "Burgos", "1980-09-24", "segundo anillo",67854623,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Alicia", "Diaz", "1996-11-22", "tres pasos al frente",650236658,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Hernan", "Moscoso", "1980-12-05", "los tusequis",3635685,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Rosa", "Martinez", "1979-07-23", "av Pirai",75658045,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Diana", "Anzoategui", "1996-06-06", " Primer anillo ",66574521,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Samuel", "Cardona", "1993-08-15", "av Alemana",79865326,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");
insert into paciente (nombre, apellido, fecha_nacimiento,direccion, telefono, created_at, updated_at)
values("Nadia", "Farias", "1991-09-26", "Av Roca y Coronado",77377340,"2020-06-16T18:10:11.311+00:00","2020-06-16T18:10:11.311+00:00");


insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital de Ninos","calle Seoane","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital Japones","tercer anillo interno","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital Catolico","Primer anillo","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital Oncologico","av Noel Kemmpff","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital Universitario","av busch","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital el Bajio","El Bajio","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital Villa Primero de mayo","villa primero de mayo","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital Frances","av Santos Dumont","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital de la Mujer","calle Rafael Penha","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");
insert into hospital (nombre,direccion,created_at,updated_at)values("Hospital San Juan de Dios","Cuellar esquina","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00");


insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Rocio","Rocha","1995-01-01"," calle libertad","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",1);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Carlos","Montalvo","1989-05--18"," calle amboro","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",2);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Ruben","Serrano","1997-09-08"," zona los pozos","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",1);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Raul","Mejia","1980-09-24"," segundo anillo","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",8);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Rodrigo","Salas","1996-11-22"," tres pasos al frente","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",5);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Carmen","Gonzales","1980-12-05"," los tusequis","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",8);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Ronald","Martinez","1979-07-23"," av Pirai","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",5);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Andrea","Zelada","1996-06-06","Primer anillo","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",7);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Esperanza","Nogales","1993-08-15"," av Alemana","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",10);
insert into doctor (nombre,apellido,fecha_nacimiento,direccion,foto,created_at,updated_at,id_hosp)values("Noelia","Nava","1991-09-26"," Av Roca y Coronado","","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",6);


insert into consulta(descripcion, fecha, created_at, updated_at,id_pac,id_doc) values("visita de rutina","2020-04-20","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",9,3);
insert into consulta(descripcion, fecha, created_at, updated_at,id_pac,id_doc) values("Problemas en los rinhones","2020-05-10","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",10,1);
insert into consulta(descripcion, fecha, created_at, updated_at,id_pac,id_doc) values("Rayos X","2020-01-19","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",5,4);
insert into consulta(descripcion, fecha, created_at, updated_at,id_pac,id_doc) values("Consulta cardiologia","2020-01-14","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",6,4);
insert into consulta(descripcion, fecha, created_at, updated_at,id_pac,id_doc) values("Rotura de ligamento","2020-06-09","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",9,7);
insert into consulta(descripcion, fecha, created_at, updated_at,id_pac,id_doc) values("visita de rutina","2020-05-02","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",8,3);
insert into consulta(descripcion, fecha, created_at, updated_at,id_pac,id_doc) values("Rayos X","2020-04-19","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",5,4);
insert into consulta(descripcion, fecha, created_at, updated_at,id_pac,id_doc) values("Dolores agudos en la rodilla","2020-04-21","2020-06-16T18:10:11.311+00:00"," 2020-06-16T18:10:11.311+00:00",3,7);

