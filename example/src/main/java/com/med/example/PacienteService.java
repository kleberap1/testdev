package com.med.example;

import java.util.List;

public interface PacienteService {
    List<Paciente> listar();
    Paciente listarPorID(int id);
    Paciente add(Paciente paciente);
    Paciente edit(Paciente paciente);
    Paciente delete(int id);
}
