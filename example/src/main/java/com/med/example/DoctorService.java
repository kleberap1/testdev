package com.med.example;

import java.util.List;

public interface DoctorService {
    List<Doctor> listar();
    Doctor listarPorID(int id);
    List<Doctor> listarPorHospId(int id);
    Doctor add(Doctor doctor);
    Doctor edit(Doctor doctor);
    Doctor delete(int id);
}
