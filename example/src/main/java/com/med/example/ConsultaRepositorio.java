package com.med.example;

import java.util.List;
import org.springframework.data.repository.Repository;

public interface ConsultaRepositorio extends Repository<Consulta, Integer>{
    List<Consulta> findAll();
    Consulta findById(int id);
    Consulta save(Consulta consulta);
    void delete(Consulta consulta);
}
