package com.med.example;

import java.util.List;

public interface HospitalService {
    List<Hospital> listar();
    Hospital listarPorID(int id);
    Hospital add(Hospital hospital);
    Hospital edit(Hospital hospital);
    Hospital delete(int id);
}
