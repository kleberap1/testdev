package com.med.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/doctores"})
public class Controlador {
    @Autowired
    DoctorService doctorService;
    @Autowired
    HospitalService hospService;
    
    @GetMapping
    public List<Doctor> listar(){
        return doctorService.listar();
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(value = "/hosp/{hospId}/")
    public ResponseEntity<?> agregar(@PathVariable (value = "hospId") int id ,@RequestBody Doctor doctor){
        Hospital hospital = hospService.listarPorID(id);
        doctor.setHospital(hospital);
        doctorService.add(doctor);
        return ResponseEntity.ok().build();
    }
    
    @GetMapping(path = {"/{id}"})
    public Doctor listarId(@PathVariable("id") int id){
        return doctorService.listarPorID(id);
    }
    
    @GetMapping("/doctores/{instructorId}/")
    public List<Doctor> listarPorHospId(@PathVariable(value = "hospId") int id) {
        return doctorService.listarPorHospId(id);
    }
    
    @PutMapping(path = {"/{id}"})
    public Doctor editar(@RequestBody Doctor doctor, @PathVariable("id") int id){
        doctor.setId(id);
        return doctorService.edit(doctor);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Doctor delete(@PathVariable("id") int id){
        return doctorService.delete(id);
    }
}
