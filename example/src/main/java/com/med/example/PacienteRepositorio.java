package com.med.example;

import java.util.List;
import org.springframework.data.repository.Repository;

public interface PacienteRepositorio extends Repository<Paciente, Integer>{
    List<Paciente> findAll();
    Paciente findById(int id);
    Paciente save(Paciente paciente);
    void delete(Paciente paciente);
}
