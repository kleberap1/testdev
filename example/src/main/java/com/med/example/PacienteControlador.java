package com.med.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/pacientes"})
public class PacienteControlador {
    @Autowired
    PacienteService pacienteService;
    
    @GetMapping
    public List< Paciente> listar(){
        return pacienteService.listar();
    }
    
    @PostMapping
    public Paciente agregar(@RequestBody  Paciente paciente){
        return pacienteService.add(paciente);
    }
    
    @GetMapping(path = {"/{id}"})
    public  Paciente listarId(@PathVariable("id") int id){
        return pacienteService.listarPorID(id);
    }
    
    @PutMapping(path = {"/{id}"})
    public  Paciente editar(@RequestBody  Paciente paciente, @PathVariable("id") int id){
        paciente.setId(id);
        return pacienteService.edit(paciente);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public  Paciente delete(@PathVariable("id") int id){
        return pacienteService.delete(id);
    }
}
