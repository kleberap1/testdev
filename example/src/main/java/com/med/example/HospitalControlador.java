package com.med.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/hospitales"})
public class HospitalControlador {
    @Autowired
    HospitalService hospitalService;
    
    @GetMapping
    public List<Hospital> listar(){
        return hospitalService.listar();
    }
    
    @PostMapping
    public Hospital agregar(@RequestBody Hospital hospital){
        return hospitalService.add(hospital);
    }
    
    @GetMapping(path = {"/{id}"})
    public Hospital listarId(@PathVariable("id") int id){
        return hospitalService.listarPorID(id);
    }
    
    @PutMapping(path = {"/{id}"})
    public Hospital editar(@RequestBody Hospital hospital, @PathVariable("id") int id){
        hospital.setId(id);
        return hospitalService.edit(hospital);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Hospital delete(@PathVariable("id") int id){
        return hospitalService.delete(id);
    }
}
