package com.med.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/consultas"})
public class ConsultaControlador {
    @Autowired
    ConsultaService consultaService;
    @Autowired
    PacienteService pacienteService;
    @Autowired
    DoctorService doctorService;
    
    @GetMapping
    public List<Consulta> listar(){
        return consultaService.listar();
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(value = "/cons/{pacId}/{docId}")
    public ResponseEntity<?> agregar(@PathVariable (value = "pacId") int id, @PathVariable (value = "docId") int idDoc
            ,@RequestBody Consulta consulta){
        Paciente paciente = pacienteService.listarPorID(id);
        Doctor doctorId = doctorService.listarPorID(idDoc);
        consulta.setPaciente(paciente);
        consulta.setDoctor(doctorId);
        consultaService.add(consulta);
        return ResponseEntity.ok().build();
    }
    
    @GetMapping(path = {"/{id}"})
    public Consulta listarId(@PathVariable("id") int id){
        return consultaService.listarPorID(id);
    }
    
    @PutMapping(path = {"/{id}"})
    public Consulta editar(@RequestBody Consulta consulta, @PathVariable("id") int id){
        consulta.setId(id);
        return consultaService.edit(consulta);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Consulta delete(@PathVariable("id") int id){
        return consultaService.delete(id);
    }
}
