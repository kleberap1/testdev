package com.med.example;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsultaServiceImp implements ConsultaService{
    @Autowired
    private ConsultaRepositorio repositorio;
    
    @Override
    public List<Consulta> listar() {
        return repositorio.findAll();
    }

    @Override
    public Consulta listarPorID(int id) {
        return repositorio.findById(id);
    }

    @Override
    public Consulta add(Consulta consulta) {
        return repositorio.save(consulta);
    }

    @Override
    public Consulta edit(Consulta consulta) {
        return repositorio.save(consulta);
    }

    @Override
    public Consulta delete(int id) {
        Consulta consulta = repositorio.findById(id);
        if (consulta != null) {
            repositorio.delete(consulta);
        }
        return consulta;
    }
    
}
