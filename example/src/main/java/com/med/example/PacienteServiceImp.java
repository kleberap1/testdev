package com.med.example;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacienteServiceImp implements PacienteService{
    @Autowired
    private PacienteRepositorio repositorio;
    @Override
    public List<Paciente> listar() {
        return repositorio.findAll();
    }

    @Override
    public Paciente listarPorID(int id) {
        return repositorio.findById(id);
    }

    @Override
    public Paciente add(Paciente paciente) {
        return repositorio.save(paciente);
    }

    @Override
    public Paciente edit(Paciente paciente) {
        return repositorio.save(paciente);
    }

    @Override
    public Paciente delete(int id) {
        Paciente paciente = repositorio.findById(id);
        if (paciente != null) {
            repositorio.delete(paciente);
        }
        return paciente;
    }
    
}
