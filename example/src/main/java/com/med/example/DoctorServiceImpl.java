package com.med.example;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoctorServiceImpl implements DoctorService{

    @Autowired
    private DoctorRepositorio repositorio;
    
    @Override
    public List<Doctor> listar() {
        return repositorio.findAll();
    }

    @Override
    public Doctor listarPorID(int id) {
        return repositorio.findById(id);
    }

    @Override
    public Doctor add(Doctor doctor) {
        return repositorio.save(doctor);
    }

    @Override
    public Doctor edit(Doctor doctor) {
        return repositorio.save(doctor);
    }

    @Override
    public Doctor delete(int id) {
        Doctor doc = repositorio.findById(id);
        if (doc != null) {
            repositorio.delete(doc);
        }
        return doc;
    }

    @Override
    public List<Doctor> listarPorHospId(int id) {
        return repositorio.findByHospitalId(id);
    }

    
}
