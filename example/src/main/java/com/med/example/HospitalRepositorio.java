package com.med.example;

import java.util.List;
import org.springframework.data.repository.Repository;

public interface HospitalRepositorio extends Repository<Hospital, Integer>{
    List<Hospital> findAll();
    Hospital findById(int id);
    Hospital save(Hospital hospital);
    void delete(Hospital hospital);
}
