package com.med.example;

import java.util.List;

public interface ConsultaService {
    List<Consulta> listar();
    Consulta listarPorID(int id);
    Consulta add(Consulta consulta);
    Consulta edit(Consulta consulta);
    Consulta delete(int id);
}
