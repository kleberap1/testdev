package com.med.example;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HospitalServiceImp implements HospitalService{

    @Autowired
    private HospitalRepositorio repositorio;
    
    @Override
    public List<Hospital> listar() {
        return repositorio.findAll();
    }

    @Override
    public Hospital listarPorID(int id) {
        return repositorio.findById(id);
    }

    @Override
    public Hospital add(Hospital hospital) {
        return repositorio.save(hospital);
    }

    @Override
    public Hospital edit(Hospital hospital) {
        return repositorio.save(hospital);
    }

    @Override
    public Hospital delete(int id) {
        Hospital hos = repositorio.findById(id);
        if (hos != null) {
            repositorio.delete(hos);
        }
        return hos;
    }
    
}
