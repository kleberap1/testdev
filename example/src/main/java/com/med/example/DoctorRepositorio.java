package com.med.example;

import java.util.List;
import org.springframework.data.repository.Repository;

public interface DoctorRepositorio extends Repository<Doctor, Integer>{
    List<Doctor> findAll();
    Doctor findById(int id);
    List<Doctor> findByHospitalId(int id);
    Doctor save(Doctor doctor);
    void delete(Doctor doctor);
}
